using System;

public interface IImage
{
    void Display();
}

public class RealImage:IImage
{
    public void Display()
    {
        Console.WriteLine("Display.");
    }

    public void Load() {}
    public void Change() {}
}

public class ProxyImage:IImage
{
    private RealImage realImage;
    public void Display()
    {
        if(realImage == null) realImage = new RealImage();
        realImage.Display();
    }
}

public class MainClass
{
    public static void Main()
    {
        IImage image = new ProxyImage();
        image.Display();
    }
}