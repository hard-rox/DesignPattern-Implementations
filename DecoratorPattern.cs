using System;

public interface IShape
{
    void Draw();
}

class Ractangle:IShape
{
    public void Draw()
    {
        Console.WriteLine("Ractangle Drawn.");
    }
}

class Triangle:IShape
{
    public void Draw()
    {
        Console.WriteLine("Triangle Drawn.");
    }
}

public abstract class ShapeDecorator:IShape
{
    protected IShape shape;
    public ShapeDecorator(IShape shape)
    {
        this.shape=shape;
    }

    public void Draw()
    {

    }
}

public class RedShapeDecorator:ShapeDecorator
{
    //protected IShape shape;
    public RedShapeDecorator(IShape shape):base(shape)
    {

    }

    public void SetRedBorder(IShape shape)
    {
        Console.WriteLine("Red Border Drawn.");
    }

    public new void Draw()
    {
        shape.Draw();
    }
}

class MainClass
{
    public static void Main()
    {
        IShape ractangle = new Ractangle();
        ractangle.Draw();

        ShapeDecorator shapeDecorator = new RedShapeDecorator(ractangle);
        //ShapeDecorator.SetRedBorder(ractangle);
        shapeDecorator.Draw();
    }
}