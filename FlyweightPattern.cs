using System;

public class Solder
{
    public void Create(){}
    public void Display(){}
    public void MoveSolder(int prevLoc, int newLoc)
    {
        Console.WriteLine("Solder moved from "+prevLoc+" to "+newLoc);
    }
}

public class SolderClient
{
    private Solder solder = new Solder();
    public void MoveSolder(int prevLoc, int newLoc)
    {
        solder.MoveSolder(prevLoc,newLoc);
    }
}

public class MainClass
{
    public static void Main()
    {
        SolderClient [] solderClient = new SolderClient[2];

        solderClient[0] = new SolderClient();
        solderClient[1] = new SolderClient();

        solderClient[0].MoveSolder(1,2);
        solderClient[1].MoveSolder(2,3);
    }
}