using System;

public interface Shape
{
    void Draw();
}

public class Ractangle:Shape
{
    public void Draw()
    {
        Console.WriteLine("Ractangle.");
    }
}

public class Triangle:Shape
{
    public void Draw()
    {
        Console.WriteLine("Triangle.");
    }
}

public class Square:Shape
{
    public void Draw()
    {
        Console.WriteLine("Square.");
    }
}

public class ShapeMaker
{
    private Ractangle ractangle;
    private Triangle triangle;
    private Square square;

    public ShapeMaker()
    {
        ractangle = new Ractangle();
        triangle = new Triangle();
        square = new Square();
    }

    public void DrawRactangle()
    {
        ractangle.Draw();
    }

    public void DrawTriangle()
    {
        triangle.Draw();
    }

    public void DrawSquire()
    {
        square.Draw();
    }
}

public class MainClass
{
    public static void Main()
    {
        ShapeMaker shapeMaker = new ShapeMaker();
        shapeMaker.DrawRactangle();
        shapeMaker.DrawTriangle();
        shapeMaker.DrawSquire();
    }
}