using System;
using System.Collections.Generic;

class User
{
    public string Name { get; set; }

    public void Update(double price)
    {
        Console.WriteLine(Name+" Updated price: "+price);
    }
}

class Company
{
    public double Price { get; set; }
    public string Name { get; set; }
    public List<User> Users { get; set; }

    public Company(string name)
    {
        this.Name=name;
        this.Price=0;
        this.Users = new List<User>();
    }

    public void SetPrice(double price)
    {
        if(price != this.Price)
        {
            Notify(price);
            this.Price=price;
        }
    }

    public void Notify(double price)
    {
        foreach (User user in Users)
        {
            user.Update(price);
        }
    }
}

class MainClass
{
    public static void Main()
    {
        Company ms=new Company("Microsoft");
        Company apple=new Company("Apple");

        ms.Users.Add(new User(){Name = "Roxy"});
        apple.Users.Add(new User(){Name = "Lota Mam"});

        ms.SetPrice(10);
        apple.SetPrice(20);
    }
}