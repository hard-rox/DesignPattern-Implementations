using System;
using System.Collections.Generic;

public class User
{
    public string Name { get; set; }

    public void Update(double price)
    {
        Console.WriteLine(Name+" Updated price: "+price);
    }
}

public static class Mediator
{
    public static void Notify(double price, List<User> users)
    {
        foreach (User user in users)
        {
            user.Update(price);
        }
    }
}

class Company
{
    public double Price { get; set; }
    public string Name { get; set; }
    public List<User> Users { get; set; }

    public Company(string name)
    {
        this.Name=name;
        this.Price=0;
        this.Users = new List<User>();
    }

    public void SetPrice(double price)
    {
        if(price != this.Price)
        {
            Mediator.Notify(price,Users);
            this.Price=price;
        }
    }
}

class MainClass
{
    public static void Main()
    {
        Company ms=new Company("Microsoft");
        Company apple=new Company("Apple");

        ms.Users.Add(new User(){Name = "Roxy"});
        apple.Users.Add(new User(){Name = "Lota Mam"});

        ms.SetPrice(10);
        apple.SetPrice(20);
    }
}